package com.example.webhook_bot.service;

import com.example.webhook_bot.bot.BotState;
import com.example.webhook_bot.cache.UserDataCache;
import com.example.webhook_bot.entity.BotUser;
import com.example.webhook_bot.repository.BotUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {
    private final Map<Long,Integer> confirmationCodesPerChat = new HashMap<>();
    private final Map<Long, BotUser> unRegisteredBotUsersWithChatId = new HashMap<>();
    private final UserDataCache userDataCache;
    private final MainMenuService mainMenuService;

    private final BotUserRepository botUserRepository;

    @Override
    public SendMessage sendMessageForStart(Long chatId) {
        BotUser botUser = botUserRepository.findBotUserByChatId(chatId).orElseGet(() ->unRegisteredBotUsersWithChatId.get(chatId));
        if(Objects.isNull(botUser)) {
            botUser = new BotUser();
            userDataCache.setUsersCurrentBotState(chatId,BotState.START);
            botUser.setChatId(chatId);
            unRegisteredBotUsersWithChatId.put(chatId, botUser);
        }
        SendMessage sendMessage = new SendMessage(chatId.toString(),"MaxWay ga Xush Kelibsiz!!!");
        sendMessage.setReplyMarkup(start());
        return sendMessage;
    }

    @Override
    public SendMessage sendMessageForLanguageChose(Long chatId){

        SendMessage sendMessage = new SendMessage( String.valueOf(chatId),"Marhamat qilib tilni tanlang");
        sendMessage.setReplyMarkup(languageChoosing());
        userDataCache.setUsersCurrentBotState(chatId,BotState.LANGUAGE_CHOOSING_BEFORE_REGISTERED);
        return sendMessage;
    }

    @Override
    public SendMessage sendMessageForContactShare(Long chatId) {
        SendMessage sendMessage = new SendMessage(chatId.toString(),"Ro'yxatdan o'tish uchun telefon raqamingizni kriting");
        sendMessage.setReplyMarkup(shareContact());
        userDataCache.setUsersCurrentBotState(chatId,BotState.CONTACT_SHARING);
        return sendMessage;
    }

    @Override
    public SendMessage sendConfirmationCode(Long chatId, String phoneNumber){
        BotUser botUser = new BotUser();
        botUser.setChatId(chatId);
        botUser.setPhoneNumber(phoneNumber);
        unRegisteredBotUsersWithChatId.put(chatId,botUser);
        confirmationCodesPerChat.put(chatId,generateAndSendCode(chatId, phoneNumber));
        SendMessage sendMessage = new SendMessage(chatId.toString(), "Telefon raqamingizga yuborilgan sms kodni kiriting. ** Kod: " + confirmationCodesPerChat.get(chatId));
        sendMessage.setReplyMarkup(backMenu());
        userDataCache.setUsersCurrentBotState(chatId,BotState.CODE_SEND);
        return sendMessage;
    }

    @Override
    public SendMessage validate(Long chatId, String code) {
        int code1;
        try {
            code1 = Integer.parseInt(code);
        }catch (Exception e){
            return sendMessageForInvalidConfirmationCode(chatId);
        }

        if(confirmationCodesPerChat.get(chatId).equals(code1))
            return register(chatId);
        else
            return sendMessageForInvalidConfirmationCode(chatId);
    }

    @Override
    public SendMessage sendMessageForInvalidConfirmationCode(Long chatId){
        SendMessage sendMessage = new SendMessage(chatId.toString(), "Kodni hato kiritdingiz !!!");
        sendMessage.setReplyMarkup(invalidCodeCase());
        if(userDataCache.getUsersCurrentBotState(chatId).equals(BotState.CODE_RESEND))
            return sendConfirmationCode(chatId,unRegisteredBotUsersWithChatId.get(chatId).getPhoneNumber());
           return sendMessage;
    }

    @Override
    public SendMessage reSendCode(Long chatId){
        confirmationCodesPerChat.put(chatId,generateAndSendCode(chatId,unRegisteredBotUsersWithChatId.get(chatId).getPhoneNumber()));
        SendMessage sendMessage = new SendMessage(chatId.toString(), "Oxirgi borgan Kodni kiriting. ** Kod: " + confirmationCodesPerChat.get(chatId));
        sendMessage.setReplyMarkup(backMenu());
        userDataCache.setUsersCurrentBotState(chatId,BotState.CODE_SEND);
        return sendMessage;
    }

    @Override
    public SendMessage register(Long chatId){
        BotUser botUser = unRegisteredBotUsersWithChatId.get(chatId);
        confirmationCodesPerChat.remove(chatId);
        unRegisteredBotUsersWithChatId.remove(chatId);
        botUserRepository.save(botUser);
        return mainMenuService.getMainMenuMessage(chatId,"Xush kelibsiz");
    }

    private Integer generateAndSendCode(Long chatId,String phoneNumber) {
        Integer code = Integer.valueOf(String.valueOf(Math.random() * 1000000).substring(0, 4));
        sendOneTimeCode(chatId, code, phoneNumber);
        return code;
    }

    private void sendOneTimeCode(Long chatId, Integer code, String phoneNumber) {

    }

    private static ReplyKeyboardMarkup shareContact() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        KeyboardRow keyboardButtons = new KeyboardRow();
        KeyboardButton keyboardButton = new KeyboardButton();
        KeyboardButton button2 = new KeyboardButton("⬅️ Orqaga");
        keyboardButton.setText("Kontaktni ulashing");
        keyboardButton.setRequestContact(true);
        keyboardButtons.add(keyboardButton);
        keyboardButtons.add(button2);
        replyKeyboardMarkup.setKeyboard(List.of(keyboardButtons));
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }

    private ReplyKeyboardMarkup backMenu() {
        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        KeyboardRow row = new KeyboardRow();
        KeyboardButton button = new KeyboardButton();
        button.setText("⬅️ Orqaga");
        row.add(button);
        markup.setSelective(true);
        markup.setResizeKeyboard(true);
        markup.setKeyboard(List.of(row));
        return markup;
    }
    private ReplyKeyboard invalidCodeCase() {
        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        KeyboardRow row = new KeyboardRow();
        KeyboardButton button1 = new KeyboardButton("Qayta yuborish");
        KeyboardButton button2 = new KeyboardButton("⬅️ Orqaga");
        row.addAll(List.of(button1,button2));
        markup.setSelective(true);
        markup.setResizeKeyboard(true);
        markup.setKeyboard(List.of(row));
        return markup;
    }


    private ReplyKeyboardMarkup languageChoosing(){
        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        KeyboardRow row = new KeyboardRow();
        KeyboardButton button1 = new KeyboardButton("O`zbekcha");
        KeyboardButton button2 = new KeyboardButton("Baribir O`zbekcha");
        KeyboardButton button3 = new KeyboardButton("⬅️ Orqaga");
        row.addAll(List.of(button1,button2,button3));
        markup.setSelective(true);
        markup.setResizeKeyboard(true);
        markup.setKeyboard(List.of(row));
        return markup;
    }

    private static ReplyKeyboardMarkup start() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        KeyboardRow keyboardButtons = new KeyboardRow();
        KeyboardButton keyboardButton = new KeyboardButton();
        keyboardButton.setText("Til tanlash");
        keyboardButtons.add(keyboardButton);
        replyKeyboardMarkup.setKeyboard(List.of(keyboardButtons));
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setSelective(true);
        return replyKeyboardMarkup;
    }


}
