package com.example.webhook_bot.cache;

import com.example.webhook_bot.bot.BotState;
import com.example.webhook_bot.entity.Basket;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserDataCache implements DataCache {
    private final Map<Long, BotState> usersBotStates = new HashMap<>();
    private final Map<Long, Basket> usersBaskets = new HashMap<>();

    @Override
    public void setUsersCurrentBotState(@NonNull Long chatId, BotState botState) {
        usersBotStates.put(chatId, botState == null? BotState.START:botState);
    }

    @Override
    public BotState getUsersCurrentBotState(@NonNull Long chatId) {
        BotState botState = usersBotStates.get(chatId);

        if (botState == null)
            botState = BotState.START;
        usersBotStates.put(chatId,botState);

        return botState;
    }

    @Override
    public Basket getUserBasket(Long chatId) {
        if (!usersBaskets.containsKey(chatId))
            usersBaskets.put(chatId, new Basket());
        return usersBaskets.get(chatId);
    }

    public boolean isUserRegistered(Long userId) {
        switch (usersBotStates.get(userId)) {
            case START:
            case CODE_SEND:
            case CONTACT_SHARING:
            case LANGUAGE_CHOOSING_BEFORE_REGISTERED:
            case CODE_RESEND:
                return true;
            default:
                return false;
        }
    }

    @Override
    public void setUserBasket(Long chatId, Basket basket) {
        usersBaskets.put(chatId, basket);
    }

}
