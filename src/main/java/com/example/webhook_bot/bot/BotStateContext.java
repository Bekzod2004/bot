package com.example.webhook_bot.bot;

import com.example.webhook_bot.bot.handler.InputMessageHandler;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BotStateContext {
    private final Map<BotState, InputMessageHandler> messageHandlers = new HashMap<>();

    public BotStateContext(List<InputMessageHandler> messageHandlers) {
        messageHandlers.forEach(
                handler -> this.messageHandlers.put(handler.getHandlerName(), handler));
    }

    public SendMessage processInputMessage(BotState currentState, Message message) {
        InputMessageHandler currentMessageHandler = findMessageHandler(currentState);
        return currentMessageHandler.handle(message);
    }

    private InputMessageHandler findMessageHandler(BotState currentState) {
        if (isOrdering(currentState))
            return messageHandlers.get(BotState.ORDER);

        if (isRegistering(currentState))
            return messageHandlers.get(BotState.START);

        if(onSettings(currentState))
            return messageHandlers.get(BotState.SETTINGS);

        return messageHandlers.get(currentState);
    }

    private boolean isOrdering(BotState currentState) {
        switch (currentState) {
            case ORDER:
            case PRODUCT:
            case CATEGORY:
                return true;
            default:
                return false;
        }
    }

    private boolean isRegistering(BotState currentState) {
        switch (currentState) {
            case START:
            case CONTACT_SHARING:
            case CODE_SEND:
            case CODE_RESEND:
            case LANGUAGE_CHOOSING_BEFORE_REGISTERED:
                return true;
            default:
                return false;
        }
    }

    private boolean onSettings(BotState currentState){
        switch (currentState){
            case SETTINGS :
            case CHANGE_LANGUAGE:
                return true;
            default: return false;
        }
    }
}





