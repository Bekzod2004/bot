package com.example.webhook_bot.service;

import com.example.webhook_bot.entity.Category;
import com.example.webhook_bot.entity.Product;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.util.List;
import java.util.Map;

public interface OrderService {

    SendMessage getAllParentCategories(Long chatId, String txt);

    SendMessage getSubCategoriesByParentName(Long chatId, String txt, String rootCategoryName, String categoryName);

    SendMessage getProductsByCategoryName(Long chatId, String txt,String categoryName);

}
