package com.example.webhook_bot.service;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public interface RegistrationService {

    SendMessage sendMessageForStart(Long chatId);
    SendMessage sendMessageForLanguageChose(Long chatId);

    SendMessage sendMessageForContactShare(Long chatId) ;

    SendMessage sendConfirmationCode(Long chatId, String phoneNumber);

    SendMessage validate(Long chatId, String code);
    SendMessage sendMessageForInvalidConfirmationCode(Long chatId);
    SendMessage reSendCode(Long chatId);

    SendMessage register(Long chatId);
}
