package com.example.webhook_bot.bot.handler;

import com.example.webhook_bot.bot.BotState;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface InputMessageHandler {
    SendMessage handle(Message message);

    BotState getHandlerName();
    void changeBotUserStateIfAnyExpectedButtonClick(Message message);
}
