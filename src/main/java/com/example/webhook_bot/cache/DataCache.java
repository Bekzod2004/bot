package com.example.webhook_bot.cache;

import com.example.webhook_bot.bot.BotState;
import com.example.webhook_bot.entity.Basket;
import lombok.NonNull;

public interface DataCache {
    void setUsersCurrentBotState(@NonNull Long userId, BotState botState);

    BotState getUsersCurrentBotState(@NonNull Long userId);

    Basket getUserBasket(Long userId);

    boolean isUserRegistered(Long userId);

    void setUserBasket(Long userId, Basket basket);
}
