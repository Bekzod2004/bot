package com.example.webhook_bot.bot.handler.registering;

import com.example.webhook_bot.bot.BotState;
import com.example.webhook_bot.bot.handler.InputMessageHandler;
import com.example.webhook_bot.cache.UserDataCache;
import com.example.webhook_bot.repository.BotUserRepository;
import com.example.webhook_bot.service.RegistrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public class RegisterHandler implements InputMessageHandler {
    private final UserDataCache userDataCache;
    private final RegistrationService registrationService;
    private final BotUserRepository botUserRepository;

    @Override
    public SendMessage handle(Message message) {
        changeBotUserStateIfAnyExpectedButtonClick(message);
        if (message.hasContact() && userDataCache.getUsersCurrentBotState(message.getChatId()).equals(BotState.CONTACT_SHARING)) {
            changeIfNeed(message.getChatId(), BotState.CONTACT_SHARING, BotState.CODE_SEND);
            return registrationService.sendConfirmationCode(message.getChatId(), message.getContact().getPhoneNumber());
        }

        switch (userDataCache.getUsersCurrentBotState(message.getChatId())) {
            case START:
                return registrationService.sendMessageForStart(message.getChatId());
            case LANGUAGE_CHOOSING_BEFORE_REGISTERED:
                return registrationService.sendMessageForLanguageChose(message.getChatId());
            case CONTACT_SHARING:
                return registrationService.sendMessageForContactShare(message.getChatId());
            case CODE_SEND:
                return registrationService.validate(message.getChatId(), message.getText());
            case CODE_RESEND:
                return registrationService.reSendCode(message.getChatId());
            case REGISTERED:
                return registrationService.register(message.getChatId());
            default:
                return new SendMessage();
        }
    }

    @Override
    public BotState getHandlerName() {
        return BotState.START;
    }


    public void changeBotUserStateIfAnyExpectedButtonClick(Message message){
        if(message.hasText())
            switch (message.getText()){
                case "Til tanlash": changeIfNeed(message.getChatId(), BotState.START,BotState.LANGUAGE_CHOOSING_BEFORE_REGISTERED); break;
                case "O`zbekcha" :
                case "Baribir O`zbekcha":
                    changeIfNeed(message.getChatId(), BotState.LANGUAGE_CHOOSING_BEFORE_REGISTERED,BotState.CONTACT_SHARING); break;
                case "Kontaktni ulashing": if(message.hasContact() && Objects.nonNull(message.getContact()))
                    changeIfNeed(message.getChatId(),BotState.LANGUAGE_CHOOSING_BEFORE_REGISTERED,BotState.CONTACT_SHARING);
            }
    }
    private void changeIfNeed(Long chatId,BotState currentState, BotState state){
        if(userDataCache.getUsersCurrentBotState(chatId).equals(currentState))
            userDataCache.setUsersCurrentBotState(chatId,state);
    }
}
