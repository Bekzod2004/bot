package com.example.webhook_bot.service;

import com.example.webhook_bot.cache.CategoryAndProductCache;
import com.example.webhook_bot.cache.UserDataCache;
import com.example.webhook_bot.entity.Category;
import com.example.webhook_bot.entity.Product;
import com.example.webhook_bot.repository.CategoryRepository;
import com.example.webhook_bot.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final UserDataCache userDataCache;

    private final CategoryAndProductCache categoryAndProductCache;

    private final CategoryRepository categoryRepository;

    private final ProductRepository productRepository;

    @Override
    public SendMessage getAllParentCategories(Long chatId, String txt) {
        SendMessage sendMessage = new SendMessage(chatId.toString(),txt);
        sendMessage.setReplyMarkup(categoriesKeyboard(categoryAndProductCache.allParentCategories()));
        return sendMessage;
    }

    @Override
    public SendMessage getSubCategoriesByParentName(Long chatId, String txt, String parentCategoryName, String categoryName) {
        SendMessage sendMessage = new SendMessage(chatId.toString(),txt);
        sendMessage.setReplyMarkup(categoriesKeyboard(categoryAndProductCache.subCategories(parentCategoryName).get(categoryName)));
        return sendMessage;
    }

    @Override
    public SendMessage getProductsByCategoryName(Long chatId, String txt,String categoryName) {
        SendMessage sendMessage = new SendMessage(chatId.toString(),txt);
        sendMessage.setReplyMarkup(productKeyboard(categoryAndProductCache.productsByCategory(categoryName)));
        return sendMessage;
    }

    private ReplyKeyboardMarkup categoriesKeyboard(Map<String,Category> map){
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setSelective(true);
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboards = new ArrayList<>();
        keyboards.add(backAndBasketRow());

        int count = 0;
        for (Map.Entry<String, Category> category: map.entrySet()){
            if(count%2 ==0){
                KeyboardRow row = new KeyboardRow();
                KeyboardButton button = new KeyboardButton(category.getValue().getNameUz());
                row.add(button);
                keyboards.add(row);
            }
            else {
                KeyboardButton button = new KeyboardButton(category.getValue().getNameUz());
                keyboards.get(keyboards.size() - 1).add(button);
            }
            count++;
        }
        keyboardMarkup.setKeyboard(keyboards);
        return keyboardMarkup;
    }

    private ReplyKeyboardMarkup productKeyboard(Map<String, Product> map){
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setSelective(true);
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboards = new ArrayList<>();
        keyboards.add(backAndBasketRow());
        int count = 0;
        for (Map.Entry<String, Product> category: map.entrySet()){
            if(count%2 ==0){
                KeyboardRow row = new KeyboardRow();
                KeyboardButton button = new KeyboardButton(category.getValue().getName());
                row.add(button);
                keyboards.add(row);
            }
            else {
                KeyboardButton button = new KeyboardButton(category.getValue().getName());
                keyboards.get(keyboards.size() - 1).add(button);
            }
            count++;
        }
        keyboardMarkup.setKeyboard(keyboards);
        return keyboardMarkup;
    }

    private KeyboardRow backAndBasketRow(){
        KeyboardRow row = new KeyboardRow();
        KeyboardButton back = new KeyboardButton("⬅️ Orqaga");
        KeyboardButton basket = new KeyboardButton("\uD83D\uDCE5 Savat");
        row.add(back);
        row.add(basket);

        return row;
    }
}
