package com.example.webhook_bot.config;

import com.example.webhook_bot.bot.FastFoodBot;
import com.example.webhook_bot.bot.TelegramFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;

@Configuration
@RequiredArgsConstructor
public class AppConfig {
    private final TelegramConfig botConfig;

    @Bean
    public FastFoodBot fastFoodBot(SetWebhook setWebhook, TelegramFacade telegramFacade) {
        FastFoodBot fastFoodBot = new FastFoodBot(setWebhook, telegramFacade);
        fastFoodBot.setBotUsername(botConfig.getBotName());
        fastFoodBot.setBotToken(botConfig.getBotToken());
        fastFoodBot.setBotPath(botConfig.getWebhookPath());
        return fastFoodBot;
    }

    @Bean
    public SetWebhook setWebhookInstance() {
        return SetWebhook.builder().url(botConfig.getWebhookPath()).build();
    }
}
