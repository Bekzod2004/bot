package com.example.webhook_bot.cache;

import com.example.webhook_bot.entity.Category;
import com.example.webhook_bot.entity.Product;

import java.util.Map;

public interface CategoryAndProductCache {
    Map<String, Category> allParentCategories();
    Map<String, Map<String, Category>> subCategories(String rootCategoryName);
    Map<String, Product> productsByCategory(String categoryName);

}
