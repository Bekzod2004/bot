package com.example.webhook_bot.bot;

public enum BotState {

    //Registration
    START,
    LANGUAGE_CHOOSING_BEFORE_REGISTERED,
    CONTACT_SHARING,
    CODE_SEND,
    CODE_RESEND,
    REGISTERED,

    // Home other
    ORDER,
    PROMOTIONS,
    ABOUT_US,
    CONTACT_US,
    SETTINGS,
    MY_ADDRESSES,

    // next to  my addresses button
    ADD_ADDRESS,
    // next to settings button
    CHANGE_LANGUAGE,

    // NEXT To Order Button
    BASKET,
    CATEGORY_FIRST,
    CATEGORY_SECOND,
    PRODUCT, CATEGORY,
    BACK,
    BUY,
}
