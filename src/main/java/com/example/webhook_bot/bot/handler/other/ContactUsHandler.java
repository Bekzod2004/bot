package com.example.webhook_bot.bot.handler.other;

import com.example.webhook_bot.bot.BotState;
import com.example.webhook_bot.bot.handler.InputMessageHandler;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Component
public class ContactUsHandler implements InputMessageHandler {
    @Override
    public SendMessage handle(Message message) {
        return null;
    }

    @Override
    public BotState getHandlerName() {
        return BotState.CONTACT_US;
    }
    public void changeBotUserStateIfAnyExpectedButtonClick(Message message){}
}
