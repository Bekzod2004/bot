package com.example.webhook_bot.repository;

import com.example.webhook_bot.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product,Integer> {
    Optional<List<Product>> findAllByCategory_NameUz(String categoryName);
}
