package com.example.webhook_bot.repository;

import com.example.webhook_bot.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category,Integer> {
    Optional<List<Category>> findAllByParentCategoryIsNull();
    Optional<List<Category>> findAllByParentCategory_NameUz(String parentCategory);

}
