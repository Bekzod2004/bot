package com.example.webhook_bot.service;

import com.example.webhook_bot.entity.Basket;
import com.example.webhook_bot.entity.BasketProduct;
import com.example.webhook_bot.entity.Category;
import com.example.webhook_bot.entity.Product;

import java.util.List;

public interface DatabaseService {

    List<Category> allParentCategories();
    List<Category> allCategoriesByRootName(String categoryName);
    List<Product> productsByCategoryName(String categoryName);
    Basket getBasketByChatId(Long chatId);
    List<BasketProduct> allBasketProductsByChatId(Long chatId);
    BasketProduct getBasketProductByChatIdAndProductName(Long chatId, String productName);
    BasketProduct addBasketProductToBasketByChatId(Long chatId, BasketProduct basketProduct);
    BasketProduct updateBasketProductOfBasketByChatId(Long chatId, BasketProduct basketProduct);
    List<BasketProduct> updateBasketProductsOfBasketByChatId(Long chatId, List<BasketProduct> basketProducts);
    void deleteBasketProductByChatIdAndName(Long chatId, String productName);
    void deleteAllBasketProductsByChatId(Long chatId);

}
