package com.example.webhook_bot.repository;

import com.example.webhook_bot.entity.BasketProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BasketProductRepository extends JpaRepository<BasketProduct,Integer> {
    Optional<List<BasketProduct>> findAllByBasket_BotUser_ChatId(Long chatId);

    Optional<BasketProduct>  findByProduct_NameAndBasket_BotUser_ChatId(String productName,Long chatId);
    void deleteAllByProduct_NameAndBasket_BotUser_ChatId(String productName, Long chatId);
    void deleteAllByBasket_BotUser_ChatId(Long chatId);
}
