package com.example.webhook_bot.repository;

import com.example.webhook_bot.entity.Basket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BasketRepository extends JpaRepository<Basket,Integer> {
    Optional<Basket> findByBotUser_ChatId(Long chatId);
}
