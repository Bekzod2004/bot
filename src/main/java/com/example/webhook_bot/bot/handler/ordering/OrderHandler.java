package com.example.webhook_bot.bot.handler.ordering;

import com.example.webhook_bot.bot.BotState;
import com.example.webhook_bot.bot.handler.InputMessageHandler;
import com.example.webhook_bot.cache.UserDataCache;
import com.example.webhook_bot.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Component
@RequiredArgsConstructor
public class OrderHandler implements InputMessageHandler {
    private final OrderService orderService;
    private final UserDataCache userDataCache;
    @Override
    public SendMessage handle(Message message) {
        switch (userDataCache.getUsersCurrentBotState(message.getChatId())){
            case ORDER:
                return rootCategories(message);
            case PRODUCT:
            case CATEGORY:
            case CATEGORY_FIRST:
            case CATEGORY_SECOND:
            case BUY:
            default:
                return new SendMessage(message.getChatId().toString(),"");
        }
    }

    @Override
    public BotState getHandlerName() {
        return BotState.ORDER;
    }
    public void changeBotUserStateIfAnyExpectedButtonClick(Message message){}
    private SendMessage rootCategories(Message message){
        return orderService.getAllParentCategories(message.getChatId(), "These are all categories");
    }

}



