package com.example.webhook_bot.cache;

import com.example.webhook_bot.entity.Category;
import com.example.webhook_bot.entity.Product;
import com.example.webhook_bot.repository.CategoryRepository;
import com.example.webhook_bot.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


@Service
@RequiredArgsConstructor
public class CategoryProductCache implements CategoryAndProductCache {
    private final Map<String, Category> categories = new HashMap<>();
    private final Map<String, Map<String, Category>> subCategories = new HashMap<>();
    private final Map<String, Map<String, Product>> products = new HashMap<>();


    private final CategoryRepository categoryRepository;

    private final ProductRepository productRepository;


    @Override
    public Map<String, Category> allParentCategories() {
        if (categories.isEmpty())
            categoryRepository.findAll().forEach(category -> categories.put(category.getNameUz(), category));
        return categories;
    }

    @Override
    public Map<String, Map<String, Category>>subCategories(String rootCategoryName) {
        if (subCategories.isEmpty()) {
            Map<String,Category> sub = new HashMap<>();
            categoryRepository.findAllByParentCategory_NameUz(rootCategoryName)
                    .get()
                    .forEach(subCategory ->sub.put(subCategory.getNameUz(), subCategory));
            subCategories.put(rootCategoryName,sub);
        }

        return subCategories;
    }

    @Override
    public Map<String, Product> productsByCategory(String categoryName) {
        if(products.isEmpty()) {
            Map<String, Product> productsWithName = new HashMap<>();
            productRepository.findAllByCategory_NameUz(categoryName).get().forEach(
                    product -> productsWithName.put(product.getName(),product));
            products.put(categoryName,productsWithName);
        }
        return products.get(categoryName);
    }
}
