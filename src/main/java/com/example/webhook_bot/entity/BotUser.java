package com.example.webhook_bot.entity;

import com.example.webhook_bot.bot.BotState;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class BotUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private Long chatId;

    @Column(nullable = false)
    private boolean isRegistered;

    @Enumerated(EnumType.STRING)
    private BotState currentState;

    private String firstName;

    private String lastName;
    @Column(nullable = false)
    private String phoneNumber;

//    private String onBotStateCategoryName;
}
