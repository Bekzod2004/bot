package com.example.webhook_bot.repository;

import com.example.webhook_bot.entity.BotUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BotUserRepository extends JpaRepository<BotUser,Integer> {
    Optional<BotUser> findBotUserByChatId(Long chatId);
//    boolean existsByChatId(Long chatId);
//    BotUser  findByRegisteredIsTrueAndChatId(Long chatId);
}
