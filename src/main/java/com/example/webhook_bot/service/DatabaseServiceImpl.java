package com.example.webhook_bot.service;

import com.example.webhook_bot.entity.*;
import com.example.webhook_bot.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class DatabaseServiceImpl implements DatabaseService {
    private final BotUserRepository botUserRepository;
    private final BasketRepository basketRepository;
    private final BasketProductRepository basketProductRepository;
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;

    @Override
    public List<Category> allParentCategories() {
        return categoryRepository.findAllByParentCategoryIsNull()
                .orElseGet(ArrayList::new);
    }

    @Override
    public List<Category> allCategoriesByRootName(String categoryName) {
        return categoryRepository.findAllByParentCategory_NameUz(categoryName)
                .orElseGet(ArrayList::new);
    }

    @Override
    public List<Product> productsByCategoryName(String categoryName) {
        return productRepository.findAllByCategory_NameUz(categoryName)
                .orElseGet(ArrayList::new);
    }

    @Override
    public Basket getBasketByChatId(Long chatId) {
        return getBasketOrCreateNew(chatId);
    }

    @Override
    public List<BasketProduct> allBasketProductsByChatId(Long chatId) {
        return basketProductRepository.findAllByBasket_BotUser_ChatId(chatId)
                .orElseGet(ArrayList::new);
    }

    @Override
    public BasketProduct getBasketProductByChatIdAndProductName(Long chatId, String productName) {
        return basketProductRepository.findByProduct_NameAndBasket_BotUser_ChatId(productName,chatId).get();
    }

    @Override
    public BasketProduct addBasketProductToBasketByChatId(Long chatId, BasketProduct basketProduct) {
        Basket basket= getBasketByChatId(chatId);

        List<BasketProduct> products = basket.getBasketProducts();
        products.add(basketProduct);
        basket.setBasketProducts(products);
        basketRepository.save(basket);
        return basketProduct;
    }

    @Override
    public BasketProduct updateBasketProductOfBasketByChatId(Long chatId, BasketProduct basketProduct) {
        Basket basket = getBasketOrCreateNew(chatId);
        basket.setBasketProducts(
                basket.getBasketProducts()
                        .stream()
                        .map(
                                basketProduct1 -> {
                                        if(basketProduct1.getProduct().getName().equals(basketProduct.getProduct().getName()))
                                            return basketProduct;
                                        return basketProduct1;
                                } )
                        .collect(Collectors.toList()));
        basketRepository.save(basket);
        return basketProduct;
    }

    @Override
    public List<BasketProduct> updateBasketProductsOfBasketByChatId(Long chatId, List<BasketProduct> basketProducts) {
        return basketProducts.stream().map(
                basketProduct ->
                        updateBasketProductOfBasketByChatId(chatId,basketProduct))
                .collect(Collectors.toList());
    }


    @Override
    public void deleteBasketProductByChatIdAndName(Long chatId, String productName) {
        basketProductRepository.deleteAllByProduct_NameAndBasket_BotUser_ChatId(productName,chatId);
    }

    @Override
    public void deleteAllBasketProductsByChatId(Long chatId) {
        basketProductRepository.deleteAllByBasket_BotUser_ChatId(chatId);
    }

    private Basket getBasketOrCreateNew(Long chatId){
        Basket basket1 =  basketRepository.findByBotUser_ChatId(chatId)
                .orElseGet(() -> {
                    Basket basket = new Basket();
                    basket.setBotUser(
                            botUserRepository.findBotUserByChatId(chatId)
                                    .orElseGet(() -> {
                                        BotUser botUser1 = new BotUser();
                                        botUser1.setChatId(chatId);
                                        return botUserRepository.save(botUser1);
                                    }));
                    return basket;
                });
        if(Objects.isNull(basket1.getId()))
            return basketRepository.save(basket1);
        return basket1;
    }
}
