package com.example.webhook_bot.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(optional = false)
    private BotUser botUser;

    @OneToMany(mappedBy = "basket", cascade = CascadeType.PERSIST)
    private List<BasketProduct> basketProducts;

}
