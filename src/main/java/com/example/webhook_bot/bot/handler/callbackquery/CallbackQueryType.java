package com.example.webhook_bot.bot.handler.callbackquery;

public enum CallbackQueryType {
    INCREMENT, DECREMENT,
    REMOVE, REMOVE_ALL
}

