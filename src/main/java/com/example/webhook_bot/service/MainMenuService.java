package com.example.webhook_bot.service;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public interface MainMenuService {
    SendMessage getMainMenuMessage(final long chatId, final String textMessage);
}
