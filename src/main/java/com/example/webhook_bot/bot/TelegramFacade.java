package com.example.webhook_bot.bot;

import com.example.webhook_bot.bot.handler.callbackquery.CallbackQueryFacade;
import com.example.webhook_bot.bot.handler.other.SettingsHandler;
import com.example.webhook_bot.bot.handler.ordering.OrderHandler;
import com.example.webhook_bot.bot.handler.registering.RegisterHandler;
import com.example.webhook_bot.cache.UserDataCache;
import com.example.webhook_bot.repository.BotUserRepository;
import com.example.webhook_bot.service.MainMenuService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class TelegramFacade {
    private final MainMenuService mainMenuService;
    private final UserDataCache userDataCache;

    private final BotUserRepository botUserRepository;
    private final BotStateContext botStateContext;
    private final CallbackQueryFacade callbackQueryFacade;
    private final RegisterHandler registerHandler;
    private final SettingsHandler settingsHandler;
    private final OrderHandler orderHandler;

    public SendMessage handleUpdate(Update update) {

        if (update.hasCallbackQuery())
            return callbackQueryFacade.processCallbackQuery(update.getCallbackQuery());

        if (update.hasMessage())
                return handleInputMessage(update.getMessage());
        return null;
    }

    private SendMessage handleInputMessage(Message message) {
        Long userId = message.getFrom().getId();
        BotState botState;
        SendMessage replyMessage;
        if(message.hasText())
            switch (message.getText()) {
                case "\uD83D\uDECD Buyurtma berish":
                    botState = BotState.ORDER;
                    break;
                case "⚙️ Sozlamalar":
                    botState = BotState.SETTINGS;
                    break;
                case "\uD83C\uDFE0 Mening manzillarim":
                    botState = BotState.MY_ADDRESSES;
                    break;
                default:
                    botState = userDataCache.getUsersCurrentBotState(userId);
            }
        else
            botState = userDataCache.getUsersCurrentBotState(userId);

        userDataCache.setUsersCurrentBotState(userId, botState);

        replyMessage = botStateContext.processInputMessage(botState, message);

        return replyMessage;
    }
}
